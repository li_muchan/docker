#!/bin/bash
#set -e
echo $JAR_FILE
echo $JAVA_MAX_MEMORY
echo $PROFILES
if [ "$JAR_FILE" = '' ]; then
	echo date
	echo "demonliyu"
	echo "JAR_FILE IS NULL"
else
	if [ -f "/app/$JAR_FILE" ]; then
		cd /app
		echo "JAR_FILE 存在!值为:$JAR_FILE"
		if [ "$JAVA_MAX_MEMORY" = "" ]; then
			echo "JAVA_MAX_MEMORY 未设置!"
			if [ "$PROFILES" = '' ]; then
				echo "PROFILES 不存在"
				java -Djava.security.egd=file:/dev/./urandom -jar $JAR_FILE
				else
				java -Djava.security.egd=file:/dev/./urandom -jar $JAR_FILE --spring.profiles.active=$PROFILES
			fi
		else
			echo "JAVA_MAX_MEMORY 已设置!值为:$JAVA_MAX_MEMORY"
			if [ "$PROFILES" = '' ]; then
				echo "PROFILES 不存在"
				java -Djava.security.egd=file:/dev/./urandom $JAVA_MAX_MEMORY -jar $JAR_FILE
				else
				echo "PROFILES 已设置!值为:$PROFILES"
				java -Djava.security.egd=file:/dev/./urandom $JAVA_MAX_MEMORY -jar $JAR_FILE --spring.profiles.active=$PROFILES
			fi

		fi
	else
		echo "/app/$JAR_FILE 路径不存在!"
	fi
fi