#!/bin/sh
set -e

# In case the user wants only to use java with options
if [ $# -gt 0 ]
then
    exec java $@
    exit 0
fi

SETENV_FILE=/app/env.sh

if [ -r $SETENV_FILE ]
then
    source $SETENV_FILE
fi


TMP_JARS_LIST=/tmp/jars_list

find /app -maxdepth 1 -regex '.*.[jw]ar$' > $TMP_JARS_LIST
nb_jars=$(cat $TMP_JARS_LIST | wc -l)
APP_ARGS="$(cat $TMP_JARS_LIST)"
if [ $nb_jars -eq 0 ]
then
    echo 'ERROR: There should be one and only one jar/war in the /app folder'
    echo '=============================================================='
    exit 1
else
    echo "jar exists:$APP_ARGS"
    if [ "$JAVA_MAX_MEMORY" = "" ]
    then
        if [ "$PROFILES" = '' ]
        then
            java -Djava.security.egd=file:/dev/./urandom -jar $APP_ARGS
        else
            java -Djava.security.egd=file:/dev/./urandom -jar $APP_ARGS --spring.profiles.active=$PROFILES
        fi
    else
        echo "JAVA_MAX_MEMORY 已设置!值为:$JAVA_MAX_MEMORY"
        if [ "$PROFILES" = '' ]
        then
           echo "PROFILES 不存在"
           java -Djava.security.egd=file:/dev/./urandom $JAVA_MAX_MEMORY -jar $APP_ARGS
        else
           echo "PROFILES 已设置!值为:$PROFILES"
           java -Djava.security.egd=file:/dev/./urandom $JAVA_MAX_MEMORY -jar $APP_ARGS --spring.profiles.active=$PROFILES
        fi
    fi
fi

exit 0
