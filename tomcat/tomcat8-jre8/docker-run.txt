docker run -d --name tomcat \
-v /opt/docker/app/tomcat/webapps/:/opt/tomcat/webapps/:z \
-v /opt/docker/app/tomcat/logs/:/opt/tomcat/logs/:z \
-v /opt/docker/app/tomcat/config/server.xml:/opt/tomcat/conf/server.xml:ro \
--net=host registry.cn-hangzhou.aliyuncs.com/kennylee/tomcat:tomcat8-jre8