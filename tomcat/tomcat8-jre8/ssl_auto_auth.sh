#/bin/sh

#续期   说明：只用renew的话，会先检查证书是否需要更新，大概是距离到期还有三天或者十几天之内才会执行更新，否则会提示不需要更新。（昨天更新了证书，今天直接用renew，提示不允许更新）
#这里方便测试，增加参数--force-renew，能够强制立即更新（但好像也会有检查，时间会变短，比如我刚才更新过了，马上再次执行会报错并提示不需要更新）。
 ./certbot-auto renew --force-renew
#生成p12
cd /mnt/web/letsTemp &&  openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out fullchain_and_key.p12 -name tomcat -passin 123456  -passout pass:123456
#移动新生成的证书文件
cp /etc/letsencrypt/live/www.rosywe.com/fullchain.pem /mnt/web/letsTemp
cp /etc/letsencrypt/live/www.rosywe.com/privkey.pem /mnt/web/letsTemp
#生成jks文件
#备份并删除原jks文件
mv /mnt/web/letsTemp/MyDSKeyStore.jks /mnt/web/letsTemp/MyDSKeyStore`date '+%Y-%m-%d'`.jks
cd /mnt/web/letsTemp && keytool -importkeystore -deststorepass 123456  -destkeypass 123456  -destkeystore MyDSKeyStore.jks -srckeystore fullchain_and_key.p12 -srcstoretype PKCS12 -srcstorepass 123456  -alias tomcat
#复制文件到tomcat目录
cp -f /mnt/web/letsTemp/MyDSKeyStore.jks /opt/docker/app/tomcat/config
#重启服务器
docker restart tomcat